from flask import Flask, request, make_response
from flask.json import jsonify

from threading import Thread
import logging

from BQservice import BigQuery
from errorNotify import SendNotify

app = Flask(__name__)



@app.route('/', methods=['POST'])
def run():
    try:
        data = request.get_json()
        run = Thread(name='createTable', target=BigQuery(data=data).CreateTable)
        run.start()

        return make_response(jsonify({'status': 'completed'}), 200)

    except Exception as err:
        logging.error(str(err))
        SendNotify().slackErr('run Thread', str(err))
        print(str(err))

        return make_response(jsonify({'status': 'failed'}), 200)


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return make_response(jsonify({'error': 'An internal error occurred'}), 500)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
