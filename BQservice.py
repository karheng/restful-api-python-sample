from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client.service_account import ServiceAccountCredentials

import datetime, time, logging, uuid, pytz

from config import BigQueryConfig
from errorNotify import SendNotify



class BigQuery(object):
    def __init__(self, data):
        self.data = data


    def _authentication(self):
        scopes = ['https://www.googleapis.com/auth/cloud-platform']

        return ServiceAccountCredentials.from_json_keyfile_name(BigQueryConfig['ServiceAccount'], scopes=scopes)


    def CreateTable(self):
        try:
            bigquery_service = build('bigquery', 'v2', credentials=self._authentication())
            job = bigquery_service.jobs()
            billProjectId = BigQueryConfig['BillProjectId']

            today = datetime.datetime.today() + pytz.timezone("Asia/Hong_Kong").utcoffset(datetime.datetime.today())
            end = (today - datetime.timedelta(self.data['enddays'])).date()
            start = (today - datetime.timedelta(self.data['startdays'])).date()
            interval = datetime.timedelta(days=1)

            i = 0
            jobIds = []
            while start <= end:
                jobId = str(uuid.uuid4())
                if i % 20 == 0:
                    time.sleep(10)
                self._query(job, BigQueryConfig['BillProjectId'], start, jobId, self.data['projectId'], self.data['datasetId'], self.data['tableId'], self.data['query'], self.data['append'], self.data['legacy'])
                start += interval
                i += 1
                jobIds.append(jobId)

            time.sleep(5)
            for jobId in jobIds:
                self._waitBQjob(job, BigQueryConfig['BillProjectId'], jobId)

        except Exception as err:
            logging.info(str(err))
            print(str(err))
            SendNotify().slackErr('bqCreateTable', str(err))


    def _query(self, job, billProjectId, date, jobId, projectId, datasetId, tableId, query, append, legacy):

            myquery = query.format(A=str((date-datetime.timedelta(1))), B=str(date).replace('-','')
                                            , C=str((date+datetime.timedelta(1))))

            data = {
                "jobReference": {
                    "projectId": billProjectId,
                    "jobId": jobId
                },
                "configuration": {
                "query": {
                    "query": myquery,
                    "destinationTable": {
                        "projectId": projectId,
                        "datasetId": datasetId,
                        "tableId": tableId.format(str(date).replace('-',''))
                    },
                    "createDisposition": "CREATE_IF_NEEDED",
                    "writeDisposition": append,
                    "allowLargeResults": True,
                    "useLegacySql": legacy
                }}}

            try:
                job.insert(projectId=billProjectId, body=data).execute()
                logging.info('Table %s Created', tableId.format(str(date).replace('-','')))

            except HttpError as err:
                logging.exception(tableId.format(str(date).replace('-','')))
                SendNotify().slackErr(tableId.format(str(date).replace('-','')), str(err))


    def _waitBQjob(self, job, billProjectId, jobId):
        result = job.get(projectId=billProjectId, jobId=jobId).execute()

        if result['status']['state'] == 'DONE':
            if 'errorResult' in result['status']:
                logging.error('{%s: %s}', result['configuration']['query']['destinationTable']['tableId'], result['status'])

                SendNotify().slackErr(result['configuration']['query']['destinationTable']['tableId'], str(result['status']['errorResult']))
