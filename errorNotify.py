import requests
from config import SlackURL


class SendNotify(object):
    def __init__(self):
        self.airasiaslack = SlackURL

    def slackErr(self, error, content):
        payload = {
            "channel": "#querycreatetable",
            "username": error,
            "icon_emoji": ":bangbang:",
            "link_names": 1,
            "attachments": [
                {
                    "fallback": "BQ query create table error",
                    "pretext": content,
                    "mrkdwn_in": ["pretext"],
                    "color": "#D00000"
                 }
              ]
        }

        requests.post(url=self.airasiaslack, json=payload)
